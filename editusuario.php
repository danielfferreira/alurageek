<?php 
  require_once "connection.php";
  require_once "src/helpers/globals.php";
  require_once "src/templates/header.php"; 
  require_once "src/dao/UserDAO.php";
  require_once "src/models/User.php";

  $userDao = new UserDAO($conn, $BASE_URL);
  $user = new User();

  $userData = $userDao->verifyToken(true);

  if($userData->image == "") {
    $userData->image = "user.png";
  }

?>

  <main class="main main--height-0 main--pt-0">
    <div class="container">

      <div class="edit__perfil">
        
        <h1 class="title__gray-default">Editar o perfil do usuário</h1>
      
        <form action="<?= $BASE_URL; ?>usuario_process.php" class="edit__form" method="POST" enctype="multipart/form-data">

          <!-- input hidden -->
          <input type="hidden" name="type" value="update">

          <!-- <p class="editar__info">Foto</p> -->
          <div class="edit__container">

            <img class="edit__imagem" style=" margin-top: 2rem; background-repeat: no-repeat; background-image: url('<?= $BASE_URL ?>src/image/usuario/<?= $userData->image ?>');">

            <div class="edit__info">
              <label>Upload imagem</label>
              <input type="file" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="image" id="image">
            </div>
          
            <div class="edit__info">
              <label>Nome</label>
              <input type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="name" id="name" value="<?= $userData->name; ?>" placeholder="Digite seu nome">
            </div>

            <div class="edit__info">
              <label>Sobrenome</label>
              <input type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="lastname" id="lastname" value="<?= $userData->lastname; ?>" placeholder="Digite seu sobrenome">
            </div>



            <div class="edit__info">
              <label>E-mail</label>
              <input type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top disabled" name="email" id="email" value="<?= $userData->email; ?>" placeholder="Digite seu e-mail">
            </div>

            <div class="edit__info">
              <label>Sobre você</label>
              <textarea type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="bio" id="bio" placeholder="Escreva mais sobre você"><?= $userData->bio; ?></textarea>
            </div>

            <div>
              <input type="submit" class="btn btn_default btn__blue" value="Alterar">
              <a href="<?= $BASE_URL; ?>carregar-senha.php" class="alterar-senha">Alterar Senha</a>
            </div>

          </div>

        </form>
        
      </div>
    </div>
  </main>

  <?php require_once "src/templates/contato.php"; ?>

<?php 
  require_once "src/templates/footer.php"; 
?>