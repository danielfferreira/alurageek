<?php 
  require_once "connection.php";
  require_once "src/helpers/globals.php";
  require_once "src/templates/header.php"; 

  require_once "src/dao/UserDAO.php";
  require_once "src/models/User.php";

  $userDao = new UserDAO($conn, $BASE_URL);
  $user = new User();

  $userData = $userDao->verifyToken(true);

?>

  <main class="main">
    <div class="container container--produto title__default">

      <h1 class="add-produto__title">Adicionar novo produto</h1>
      <span>Aqui você pode adicionar seu produto favorito na loja <span class="alura">Alura</span><span class="geek">Geek</span></span>

      <form action="<?= $BASE_URL; ?>produto_process.php" class="edit__form" method="POST" enctype="multipart/form-data">

          <!-- input hidden -->
          <input type="hidden" name="type" value="create">

          <!-- <p class="editar__info">Foto</p> -->
          <div class="edit__container">

            <!-- <img class="edit__imagem" style=" margin-top: 2rem; background-repeat: no-repeat; background-image: url('<?= $BASE_URL ?>src/image/usuario/<?= $userData->image ?>');"> -->

            <div class="edit__info">
              <label>Upload imagem</label>
              <input type="file" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="image" id="image">
            </div>
          
            <div class="edit__info">
              <label>categoria</label>
              <select type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="category" id="category">
                <option value="">Selecione</option>
                <option value="Consoles">Consoles</option>
                <option value="Camisetas">Camisetas</option>
                <option value="Computadores">Computadores</option>
                <option value="Mouse">Mouse</option>
                <option value="CadeiraGamer">Cadeira gamer</option>
                <option value="Monitor">Monitor</option>
                <option value="Bonecos">Star Wars</option>
                <option value="BonecosPok">Pokemon</option>
                <option value="BonecosSonic">Sonic</option>
              </select>
            </div>

            <div class="edit__info">
              <label>Nome do produto</label>
              <input type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="name" id="name" value="" placeholder="Digite o nome do produto">
            </div>

            <div class="edit__info">
              <label>Coleção do produto</label>
              <input type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="collection" id="collection" value="" placeholder="Digite a coleção do produto">
            </div>

            <div class="edit__info">
              <label>Preço do produto</label>
              <input type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="price" id="price" value="" placeholder="Digite o preço do produto">
            </div>

            <div class="edit__info">
              <label>Descrição do produto</label>
              <textarea type="text" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="description" id="description" placeholder="Digite a descrição do produto"></textarea>
            </div>

            <div>
              <input type="submit" class="btn btn_default btn__blue" value="Adicionar produto">
            </div>

          </div>

          

        </form>

    </div>
  </main>

  <?php require_once "src/templates/contato.php"; ?>

<?php 
  require_once "src/templates/footer.php"; 
?>