<?php

  require_once "src/templates/header.php";

  // verifica se usuário está autenticado
  require_once "src/models/Product.php";
  require_once "src/dao/ProductDAO.php";

  // pegar o id do produto
  $id = filter_input(INPUT_GET, "id");

  $product;

  $productDao = new ProductDAO($conn, $BASE_URL);
  
  if(empty($id)) {

    // enviar menssagem de erro, o produto não foi encontrado
    $message->setMessage("O produto não foi encontrado", "error", "index.php");

  } else {

    $product = $productDao->findById($id);

    // verificar se o filme existe
    if(!$product) {

      // enviar menssagem de erro, o produto não foi encontrado
      $message->setMessage("O produto não foi encontrado", "error", "index.php");

    }

  }

?>