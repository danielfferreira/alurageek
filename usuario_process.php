<?php

/*
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(E_ALL);
*/

// echo "<pre>"; print_r($_POST); print_r($_FILES); echo "</pre>"; exit;

require_once "src/helpers/globals.php";
require_once "connection.php";
require_once "src/models/User.php";
require_once "src/models/Message.php";
require_once "src/dao/UserDAO.php";

$message = new Message($BASE_URL);

$userDao = new UserDAO($conn, $BASE_URL);

// Resgata o tipo do formulário
$type = filter_input(INPUT_POST, "type");

// Atualizar usuário
if($type === "update") {

  // Resgata dados do usuário
  $userData = $userDao->verifyToken();

  // Receber dados do post
  $name = filter_input(INPUT_POST, "name");
  $lastname = filter_input(INPUT_POST, "lastname");
  $email = filter_input(INPUT_POST, "email");
  $bio = filter_input(INPUT_POST, "bio");


  // print_r($name);
  // echo "<br>";
  // print_r($lastname); 
  // echo "<br>";
  // print_r($email); 
  // echo "<br>";
  // print_r($bio); 
  // echo "<br>";
  // print_r($banner);
  // exit;

  // Criar um novo objeto de usuário
  $user = new User();

  // Preencher os dados do usuário
  $userData->name = $name;
  $userData->lastname = $lastname;
  $userData->email = $email;
  $userData->bio = $bio;

  /* 
    upload da imagem - AluraGeek
  */ 

  if(
    isset($_FILES['image']) && 
    !empty($_FILES['image']['tmp_name'])
  ) {

    $image = $_FILES['image'];
    
    $imageTypes = ["image/jpeg", "image/jpg", "image/png"];

    $jpgArray = ["image/jpeg", "image/jpg"];

    

    // checagem de tipo da imagem
    if(in_array($image['type'], $imageTypes)) {


      // checa se é jpg
      if(in_array($image, $jpgArray)) {

        $imageFile = imagecreatefromjpeg($image['tmp_name']);

        // checa se é png
      } else {

        $imageFile = imagecreatefrompng($image['tmp_name']);

      }

      // gera o nome para o arquivo
      $imageName = $user->imageGenerateName();

      imagejpeg($imageFile, "src/image/usuario/" . $imageName, 100);

      $userData->image = $imageName;

    } else {

      // enviar mensagem de erro, Tipo inválido de imagem, insira png ou jpg!
      $message->setMessage("Tipo inválido de imagem, insira png ou jpg!.", "error", "back");

    }

  }

  $userDao->update($userData);

// Atualizar senha do usuário
} else if($type === "changepassword") {

  // Receber dados do post
  $password = filter_input(INPUT_POST, "password");
  $confirmpassword = filter_input(INPUT_POST, "confirmpassword");
  
  // Resgata dados do usuário pelo token
  $userData = $userDao->verifyToken();
  $id = $userData->id;

  // conferir se as senha batem (conferem)
  if($password == $confirmpassword) {

    // Verificar se existe os campos vázios
    if(!empty($password) OR !empty($confirmpassword)) {

      // Verificar se a senha, têm no mínimo 6 caracteres
      if(preg_match('/^[\w$@]{6,}$/', $password)) {

        // Criar um novo objeto de usuário
        $user = new User();

        $finalPassword = $user->generatePassword($password);

        $user->password = $finalPassword;
        $user->id = $id;

        $userDao->changePassword($user);


      } else {

        // enviar menssagem de erro, senhas ter no mínimo 6 caracteres
        $message->setMessage("As senhas têm que ter no mínimo, de 6 caracteres", "error", "back");

      }

    } else {

      // enviar menssagem de erro, senhas ter no mínimo 6 caracteres
      $message->setMessage("O campo senha ou confirma senha, está vazio. Por favor volte e preencha todos novamente.", "error", "back");

    }

  } else {

    // enviar mensagem de erro, As senhas não são iguais!
    $message->setMessage("As senhas não são iguais!.", "error", "back");

  }

} else {

  // enviar mensagem de erro, Informações inválidas!
  $message->setMessage("Informações inválidas!.", "error", "index.php");

}

?>


