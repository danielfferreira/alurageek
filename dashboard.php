<?php
  require_once "src/templates/header.php"; 

  require_once "src/models/User.php";
  require_once "src/dao/UserDAO.php";
  require_once "src/dao/ProductDAO.php";

  // DAO dos produtos
  $user = new User();
  $userDao = new UserDAO($conn, $BASE_URL);
  $productDao = new ProductDAO($conn, $BASE_URL);

  $userData = $userDao->verifyToken(true);

  // $latesProduct = $productDao->getLatesProducts();
  $userProducts = $productDao->getProductsByUserId($userData->id);

?>

  <main class="main">
    <div class="container container--dashboard title__default">
      <div class="dashboard__header-title">
        <h1 class="add-produto__title">Todos os produtos</h1>
        <a href="<?= $BASE_URL; ?>newproduto.php" class="btn btn_add-product">Adicionar produto</a>
      </div>

      <?php if(isset($userProducts)) { ?>
        <div class="box-products box-products--dashboard">
      
          <div class="box-products--imgs box-dashboard--imgs">
            <?php foreach($userProducts as $product): ?>
              <div>
                <img src="<?php echo $BASE_URL ?>src/image/products/<?php echo $product->image; ?>" alt="imagem dos produtos">

                <div class="box__prod-edit-del">
                  <form action="<?php echo $BASE_URL ?>produto_process.php">
                    <input type="hidden" name="type" value="delete">
                    <input type="hidden" name="id" value="<?php echo $product->id; ?>">
                    <button type="submit" class="btn-del-prod">
                      <i class="ri-delete-bin-7-fill" title="Deletar"></i>
                    </button>
                  </form>
                  <a href="<?php echo $BASE_URL ?>editproduct.php?id=<?php echo $product->id; ?>">
                    <i class="ri-pencil-fill" title="Editar"></i>
                  </a>
                </div>
                <p class="description">
                  <?php 
                    $productName = $product->name;
                    echo wordwrap($productName, 25, "<br/> \n"); 
                  ?>
                </p>
                <p class="description description--price"><?php echo $product->price; ?></p>
                <p class="description">
                  #
                  <?php echo $product->id;?>
                </p>
              </div>
            <?php endforeach; ?>
          </div>
          <?php if(count($userProducts) === 0): ?>
            <p class="description description--null-product">Ainda não há produtos cadastrados!</p>
          <?php endif; ?>
        </div>
      <?php } ?>

    </div>
  </main>
    

  <?php require_once "src/templates/contato.php"; ?>


<?php
  require_once "src/templates/footer.php"; 
?>