<?php
require_once "connection.php";
require_once "src/helpers/globals.php";
require_once "src/templates/header.php";

?>

<main class="main main--autenticacao">
  <div class="container container--autenticacao login">
    <form action="<?= $BASE_URL ?>autenticacao_process.php" method="POST" class="login__form">

      <input type="hidden" name="type" value="register">

      <fieldset>
        <!-- <legend>Criar conta</legend> -->

        <div class="login__container">

          <label for="email">E-mail</label>
          <input type="text" class="login__autenticacao login__autenticacao--m-bottom" name="email" placeholder="Digite seu e-mail">

          <label for="name">Nome</label>
          <input type="text" class="login__autenticacao login__autenticacao--m-bottom" name="name" placeholder="Digite seu nome">

          <label for="lastname">Sobrenome</label>
          <input type="text" class="login__autenticacao login__autenticacao--m-bottom" name="lastname" placeholder="Digite seu sobrenome">

          <label for="password">Senha</label>
          <input type="password" class="login__autenticacao login__autenticacao--m-bottom" name="password" placeholder="Digite sua senha">

          <label for="confirmpassword">Confirmação de senha</label>
          <input type="password" class="login__autenticacao login__autenticacao--m-bottom" name="confirmpassword" placeholder="Confirme sua senha">

          <button type="submit" class="btn btn_default btn__blue">Confirmar</button>

        </div>

      </fieldset>
    </form>
  </div>
</main>

<?php
require_once "src/templates/footer.php";
?>