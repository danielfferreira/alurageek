<?php

  define('HOST', 'localhost');
  define('USER', 'root');
  define('PASS', '');
  define('DBNAME', 'alurageek');


  //Criar a conexão com banco de dados usando o PDO e a porta do banco de dados
  //Utilizar o Try/Catch para verificar a conexão.
  try {
    $conn = new pdo('mysql:host=' . HOST . ';dbname=' . DBNAME, USER, PASS);
    // echo "Conexão com banco de dados realizada com sucesso.";
  } catch (PDOException $e) {
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  }


?>