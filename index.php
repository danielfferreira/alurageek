<?php 

  // ini_set('display_startup_errors',1);
  // ini_set('display_errors',1);
  // error_reporting(E_ALL);

  
  require_once "src/templates/header.php";  
  require_once "src/dao/ProductDAO.php";

  // DAO dos produtos
  $productDao = new ProductDAO($conn, $BASE_URL);

  $latesProduct = $productDao->getLatesProducts();
  // echo "<pre>"; print_r($latesProduct); echo "</pre>";

  $consoleProduct = $productDao->getProductsCategory("Consoles");
  $shirtsProduct = $productDao->getProductsCategory("Camisetas");
  $computerProduct = $productDao->getProductsCategory("Computadores");
  $mouseProduct = $productDao->getProductsCategory("Mouse");
  $gamerchairProduct = $productDao->getProductsCategory("CadeiraGamer");
  $monitorProduct = $productDao->getProductsCategory("Monitor");
  $dollsProduct = $productDao->getProductsCategory("Bonecos");
  $dollsPokemonProduct = $productDao->getProductsCategory("BonecosPok");
  $dollsSonicProduct = $productDao->getProductsCategory("BonecosSonic");
  
  // echo "<pre>"; print_r($consoleProduct); echo "</pre>"; 

?>
  

  <main class="main title__default">

    <div class="banner">
      <!--=============== BANNER - Home ===============-->
      <img src="src/image/banner.jpg" alt="banner">

      <div class="banner__promocao">
        <h2>Dezembro Promocional</h2>
        <p>Produtos selecionados com 33% de desconto</p>
        <div class="banner--mt">
          <a href="#" class="btn btn__blue btn_default">Ver Consoles</a>
        </div>
      </div>

    </div>
  
    <div class="container">
    
      <div class="box-products box-products--consoles">
        <div class="box-products-container">
          <span>Star wars</span>
          <a href="<?= $BASE_URL; ?>dashboard.php" class="see-it-all">
            Ver tudo
            <i class="ri-arrow-right-line"></i>
          </a>
        </div>
        <p class="description">Veja os melhores produtos da linha Star wars</p>
        <div class="box-products--imgs">
          <?php foreach($dollsProduct as $product): ?>
            <div>
              <img src="<?php echo $BASE_URL ?>src/image/products/<?php echo $product->image; ?>" alt="imagem dos produtos"> 
              <p class="description">
                <?php 
                  $productName = $product->name;
                  echo wordwrap($productName, 30, "<br/> \n"); 
                ?>
              </p>
              <p class="description description--price"><?php echo $product->price; ?></p>
              <a href="<?php echo $BASE_URL ?>produto.php" class="see-it-all">
                Ver produto
              </a>
            </div>
          <?php endforeach; ?>
        </div>
        <?php if(count($dollsProduct) === 0): ?>
          <p class="description description--null-product">Ainda não há produtos cadastrados!</p>
        <?php endif; ?>
      </div>

      <div class="box-products box-products--consoles">
        <div class="box-products-container">
          <span>Consoles</span>
          <a href="<?= $BASE_URL; ?>dashboard.php" class="see-it-all">
            Ver tudo
            <i class="ri-arrow-right-line"></i>
          </a>
        </div>
        <p class="description">Veja os melhores produtos da linha de consoles</p>
        <div class="box-products--imgs">
          <?php foreach($consoleProduct as $product): ?>
            <div>
              <img src="<?php echo $BASE_URL ?>src/image/products/<?php echo $product->image; ?>" alt="imagem dos produtos">
              <p class="description">
                <?php 
                  $productName = $product->name;
                  echo wordwrap($productName, 30, "<br/> \n"); 
                ?>
              </p>
              <p class="description description--price"><?php echo $product->price; ?></p>
              <a href="#" class="see-it-all">
                Ver produto
              </a>
            </div>
          <?php endforeach; ?>
        </div>
        <?php if(count($consoleProduct) === 0): ?>
          <p class="description description--null-product">Ainda não há produtos cadastrados!</p>
        <?php endif; ?>
      </div>

      <div class="box-products box-products--consoles">
        <div class="box-products-container">
          <span>Pokemon</span>
          <a href="<?= $BASE_URL; ?>dashboard.php" class="see-it-all">
            Ver tudo
            <i class="ri-arrow-right-line"></i>
          </a>
        </div>
        <p class="description">Veja os melhores produtos da coleção pokemon</p>
        <div class="box-products--imgs">
          <?php foreach($dollsPokemonProduct as $product): ?>
            <div>
              <img src="<?php echo $BASE_URL ?>src/image/products/<?php echo $product->image; ?>" alt="imagem dos produtos">
              <p class="description"><?php echo $product->name; ?></p>
              <p class="description description--price"><?php echo $product->price; ?></p>
              <a href="#" class="see-it-all">
                Ver produto
              </a>
            </div>
          <?php endforeach; ?>
        </div>
        <?php if(count($dollsPokemonProduct) === 0): ?>
          <p class="description description--null-product">Ainda não há produtos cadastrados!</p>
        <?php endif; ?>
      </div>

      <div class="box-products box-products--consoles">
        <div class="box-products-container">
          <span>Atari</span>
          <a href="<?= $BASE_URL; ?>dashboard.php" class="see-it-all">
            Ver tudo
            <i class="ri-arrow-right-line"></i>
          </a>
        </div>
        <p class="description">Veja os melhores produtos da linha atari</p>
        <div class="box-products--imgs">
          <?php foreach($shirtsProduct as $product): ?>
            <div>
              <img src="<?php echo $BASE_URL ?>src/image/products/<?php echo $product->image; ?>" alt="imagem dos produtos">
              <p class="description"><?php echo $product->name; ?></p>
              <p class="description description--price"><?php echo $product->price; ?></p>
              <a href="#" class="see-it-all">
                Ver produto
              </a>
            </div>
          <?php endforeach; ?>
        </div>
        <?php if(count($shirtsProduct) === 0): ?>
          <p class="description description--null-product">Ainda não há produtos cadastrados!</p>
        <?php endif; ?>
      </div>

      <div class="box-products box-products--consoles">
        <div class="box-products-container">
          <span>Sonic</span>
          <a href="<?= $BASE_URL; ?>dashboard.php" class="see-it-all">
            Ver tudo
            <i class="ri-arrow-right-line"></i>
          </a>
        </div>
        <p class="description">Veja os melhores produtos da linha Sonic</p>
        <div class="box-products--imgs">
          <?php foreach($dollsSonicProduct as $product): ?>
            <div>
              <img src="<?php echo $BASE_URL ?>src/image/products/<?php echo $product->image; ?>" alt="imagem dos produtos">
              <p class="description"><?php echo $product->name; ?></p>
              <p class="description description--price"><?php echo $product->price; ?></p>
              <a href="#" class="see-it-all">
                Ver produto
              </a>
            </div>
          <?php endforeach; ?>
        </div>
        <?php if(count($dollsSonicProduct) === 0): ?>
          <p class="description description--null-product">Ainda não há produtos cadastrados!</p>
        <?php endif; ?>
      </div>
      

    </div>

  </main>
  <?php require_once "src/templates/contato.php"; ?>

<?php 
  require_once "src/templates/footer.php"; 
?>