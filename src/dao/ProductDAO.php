<?php

require_once "src/models/Product.php";
require_once "src/models/Message.php";


class ProductDAO implements ProductDAOInterface {

  private $conn;
  private $url;
  private $message;

  public function __construct(PDO $conn, $url)
  {
    $this->conn = $conn;
    $this->url = $url;
    $this->message = new Message($url);
  }


  public function buildProduct($data) {

    $product = new Product();

    $product->id = $data["id"];
    $product->name = $data["name"];
    $product->price = $data["price"];
    $product->image = $data["image"];
    $product->category = $data["category"];
    $product->description = $data["description"];
    $product->collection = $data["collection"];
    $product->users_id = $data["users_id"];

    return $product;

  }


  public function findAll() {

  }


  public function getLatesProducts() {

    $products = [];

    $stmt = $this->conn->prepare("SELECT * FROM tbl_produtos ORDER BY id");

    $stmt->execute();

    if($stmt->rowCount() > 0) {
      $productsArray = $stmt->fetchAll();

      foreach($productsArray as $product) {
        $products[] = $this->buildProduct($product);
      }

    }


    return $products;

  }


  public function getProductsCategory($category) {

    $products = [];

    $stmt = $this->conn->prepare("SELECT * FROM tbl_produtos
                                  WHERE category = :category
                                  ORDER BY id");

    $stmt->bindParam(":category", $category);

    $stmt->execute();

    if($stmt->rowCount() > 0) {
      $productsArray = $stmt->fetchAll();

      foreach($productsArray as $product) {
        $products[] = $this->buildProduct($product);
      }

    }

    return $products;
  }


  public function getProductsByUserId($id) {

    $products = [];

    $stmt = $this->conn->prepare("SELECT * FROM tbl_produtos  
                                  WHERE users_id = :users_id");

    $stmt->bindParam(":users_id", $id);

    $stmt->execute();

    if($stmt->rowCount() > 0) {

      $productsArray = $stmt->fetchAll();

      foreach($productsArray as $product) {
        $products[] = $this->buildProduct($product);
      }

    }

    return $products;

  }


  public function findById($id) {

    $product = [];

    $stmt = $this->conn->prepare("SELECT * FROM tbl_produtos
                                  WHERE id = :id");

    $stmt->bindParam(":id", $id);

    $stmt->execute();

    if($stmt->rowCount() > 0) {

      $productData = $stmt->fetch();

      $product = $this->buildProduct($productData);

      return $product;

    } else {

      return false;

    }

  }


  public function findByTitle($title) {

  }


  public function create(Product $product) {

    $stmt = $this->conn->prepare("INSERT INTO tbl_produtos (
      name, price, image, category, description, collection, users_id
    ) VALUES (
      :name, :price, :image, :category, :description, :collection, :users_id
    )");

    $stmt->bindParam(":name", $product->name);
    $stmt->bindParam(":price", $product->price);
    $stmt->bindParam(":image", $product->image);
    $stmt->bindParam(":category", $product->category);
    $stmt->bindParam(":description", $product->description);
    $stmt->bindParam(":collection", $product->collection);
    $stmt->bindParam(":users_id", $product->users_id);

    // print "<pre>"; print_r($stmt); print "</pre>"; exit;
    
    $stmt->execute();

    // enviar mensagem de sucesso, Produto adicionado com sucesso
    $this->message->setMessage("Produto adicionado com sucesso!.", "success", "index.php");

  }


  public function update(Product $Product) {

  }


  public function destroy($id) {

  }



}

?>