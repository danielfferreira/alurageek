<div class="contato">
  <div class="contato-content container">

    <!--=============== LOGO ===============-->
    <a href="<?= $BASE_URL; ?>  " class="img__fluid img__fluid--width">
      <img src="src/image/logo/Logo.png" alt="Logo alura geek - loja de produtos para video game e outros">
    </a>

    <!--=============== CONTATO ===============-->
    <ul class="contato__nav">
      <li>
        <a href="#">Quem somos nós</a>
      </li>
      <li>
        <a href="#">Política de privacidade</a>
      </li>
      <li>
        <a href="#">Programa fidelidade</a>
      </li>
      <li>
        <a href="#">Nossas lojas</a>
      </li>
      <li>
        <a href="#">Quero ser franqueado</a>
      </li>
      <li>
        <a href="#">Anuncie aqui</a>
      </li>
    </ul>


  </div>
</div>