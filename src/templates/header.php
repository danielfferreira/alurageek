<?php 
  require_once "src/helpers/globals.php"; 
  require_once "connection.php";
  require_once "src/models/Message.php";
  require_once "src/dao/UserDAO.php";

  $message = new Message($BASE_URL);
  $userDao = new UserDAO($conn, $BASE_URL);
  $userData = $userDao->verifyToken(false);

  $Msg = $message->getMessage();

  if(!empty($Msg["message"])) {
    // Limpar a mensagem
    $message->clearMessage();
  }
  
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--=============== FAVICON ===============-->
  <link rel="shortcut icon" href="src/image/pacman.svg" type="image/x-icon">

  <!--=============== REMIXICONS ===============-->
  <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

  <!--=============== GOOGLE FONTS - Raleway ===============-->
  <link
      href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700&family=Press+Start+2P&display=swap"
      rel="stylesheet"
  />

  <!--=============== CSS ===============-->
  <link rel="stylesheet" href="<?= $BASE_URL; ?>src/css/style.css">

  <title>Alura geek</title>
</head>
<body>
  
  <!--=============== HEADER ===============-->
  <header class="header">

    <!--=============== NAV ===============-->
    <nav class="nav container">

      <!--=============== LOGO ===============-->
      <a href="<?= $BASE_URL; ?>  " class="img__fluid">
        <img src="src/image/logo/Logo.png" alt="Logo alura geek - loja de produtos para video game e outros">
      </a>

      <!--=============== PESQUISAR ===============-->
      <!-- <div class="search-box">
        <input class="input_search" type="text" placeholder="O que deseja encontrar?">
        <a href="#" class="btn-search">
          <img src="src/image/lupa.png" alt="Icone lupa (pesquisar)">
        </a>
      </div> -->

      <!--=============== BUTTON LOGIN ===============-->

      <div class="nav__autenticacao">
        <?php if($userData): ?>
          <ul class="nav__list">
            <li><a href="<?= $BASE_URL; ?>editusuario.php" class="nav__usuario"><?= $userData->name; ?></a></li>
            <li><a href="<?= $BASE_URL; ?>logout.php" class="btn btn_default btn_sair">Sair</a></li>
          </ul>
        <?php else: ?>
          <a class="btn btn_default btn_default--login" href="<?= $BASE_URL; ?>autenticacao.php">
            Login
          </a>
          <a href="<?= $BASE_URL; ?>create_autenticacao.php">
            Criar conta
          </a>
        <?php endif; ?>
      </div>

    </nav>
    <!--=============== FIM NAV ===============-->

  </header>
  <!--=============== FIM HEADER ===============-->
  <?php if(!empty($Msg["message"])): ?>
    <div class="message-container">
      <p class="message <?= $Msg['type']; ?>"><?= $Msg['message']; ?></p>
    </div>
  <?php endif; ?>