  <footer class="footer title__default">
    <p class="description">Desenvolvido por Daniel Ferreira <br> <?= date('Y'); ?></p>
  </footer>

  <!--=============== JQUERY ===============-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <!--=============== JS ===============-->
  <script src="src/js/script.js"></script>
</body>
</html>