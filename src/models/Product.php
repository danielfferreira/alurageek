<?php 

  class Product {

    public $id;
    public $name;
    public $price;
    public $image;
    public $category;
    public $description;
    public $collection;
    public $users_id; 


    public function imageGenerateName() {
      return bin2hex(random_bytes(60)) . ".jpg";
    }

  }

  interface ProductDAOInterface {

    public function buildProduct($data);
    public function findAll();
    public function getLatesProducts();
    public function getProductsCategory($category);
    public function getProductsByUserId($id);
    public function findById($id);
    public function findByTitle($title);
    public function create(Product $product);
    public function update(Product $product);
    public function destroy($id);

  }

?>