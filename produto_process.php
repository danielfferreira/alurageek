<?php 

// ini_set('display_startup_errors',1);
// ini_set('display_errors',1);
// error_reporting(E_ALL);


require_once "src/helpers/globals.php";
require_once "connection.php";
require_once "src/models/Product.php";
require_once "src/models/Message.php";
require_once "src/dao/UserDAO.php";
require_once "src/dao/ProductDAO.php";

$message = new Message($BASE_URL);
$userDao = new UserDAO($conn, $BASE_URL);
$productDao = new ProductDAO($conn, $BASE_URL);

// Resgata o tipo do formulário
$type = filter_input(INPUT_POST, "type");

// Resgata dados do usuário
$userData = $userDao->verifyToken();
// print_r($userData); exit;

if($type === "create") {

  // print_r("Caiu aqui, dentro do create"); exit;

  // receber os dados dos inputs (POST)
  $name = filter_input(INPUT_POST, "name");
  $price = filter_input(INPUT_POST, "price");
  $category = filter_input(INPUT_POST, "category");
  $description = filter_input(INPUT_POST, "description");
  $collection = filter_input(INPUT_POST, "collection");

  // criar objeto produtos
  $product = new Product();


  // validação mínima dos dados
  if(!empty($name) && !empty($price) && !empty($description)) { 

    $product->name = $name;
    $product->price = $price;
    $product->category = $category;
    $product->description = $description;
    $product->collection = $collection;
    $product->users_id = $userData->id;

    // echo "<pre>"; print_r($product); echo "</pre>"; exit;

    // upload de imagem do produto
    // Upload de imagem do filme
    if(isset($_FILES["image"]) && !empty($_FILES["image"]["tmp_name"])) {

      $image = $_FILES["image"];
      $imageTypes = ["image/jpeg", "image/jpg", "image/png"];
      $jpgArray = ["image/jpeg", "image/jpg"];

      // Checando tipo da imagem
      if(in_array($image["type"], $imageTypes)) {

        // Checa se imagem é jpg
        if(in_array($image["type"], $jpgArray)) {
          $imageFile = imagecreatefromjpeg($image["tmp_name"]);
        } else {
          $imageFile = imagecreatefrompng($image["tmp_name"]);
        }

        // Gerando o nome da imagem
        $imageName = $product->imageGenerateName();

        imagejpeg($imageFile, "src/image/products/" . $imageName, 100);

        
        $product->image = $imageName;

      } else {

        $message->setMessage("Tipo inválido de imagem, insira png ou jpg!", "error", "back");

      }

    }

    // echo "<pre>"; print_r($_POST); print_r($_FILES); echo "</pre>"; exit;

    $productDao->create($product);

  } else {

    // enviar mensagem de erro, Informações inválidas!
    $message->setMessage("Você precisa adicionar pelo menos: nome do produto, preço do produto e descrição do produto.", "error", "back");

  }

} else {

  // enviar mensagem de erro, Informações inválidas!
  $message->setMessage("Informações inválidas!.", "error", "index.php");

}


?>