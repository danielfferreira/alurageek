<?php 
  require_once "connection.php";
  require_once "src/helpers/globals.php";
  require_once "src/templates/header.php"; 
  require_once "src/dao/UserDAO.php";
  require_once "src/models/User.php";

  $userDao = new UserDAO($conn, $BASE_URL);
  $user = new User();

  $userData = $userDao->verifyToken(true);

  // if($userData->image == "") {
  //   $userData->image = "user.png";
  // }

?>

  <main class="main main--height-0 main--pt-0">
    <div class="container">
      <h1 class="edit__titulo">Alterar senha do usuário</h1>
      <div class="edit__perfil">
        
        <form action="<?= $BASE_URL; ?>usuario_process.php" class="edit__form" method="POST">

          <!-- input hidden -->
          <input type="hidden" name="type" value="changepassword">
          <input type="hidden" name="id" value="<?= $userData->id; ?>">

          <!-- <p class="editar__info">Foto</p> -->
          <div class="edit__container">
          
            <div class="edit__info">
              <label for="password">Senha</label>
              <input type="password" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="password" id="password" placeholder="Digite a sua nova senha">
            </div>

            <div class="edit__info">
              <label for="confirmpassword">Confirmarção de Senha</label>
              <input type="password" class="login__autenticacao login__autenticacao--editUsuario login__autenticacao--m-top" name="confirmpassword" id="confirmpassword" placeholder="Confirme a sua nova senha">
            </div>

            <input type="submit" class="btn btn_default btn__blue" value="Alterar Senha">
            <!-- <a href="<?= $BASE_URL; ?>carregar-senha.php" class="alterar-senha">Alterar Senha</a> -->

          </div>

          

        </form>
      </div>
    </div>
  </main>

  <?php require_once "src/templates/contato.php"; ?>

<?php 
  require_once "src/templates/footer.php"; 
?>