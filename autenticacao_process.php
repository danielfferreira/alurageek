<?php

  require_once "src/models/User.php";
  require_once "src/dao/UserDAO.php";
  require_once "src/helpers/globals.php"; 
  require_once "src/models/Message.php";
  require_once "connection.php";

  $message = new Message($BASE_URL);

  $userDao = new UserDAO($conn, $BASE_URL);

  // resgata o tipo do formulario
  $type = filter_input(INPUT_POST, "type");

  // verificação do tipo de formulário
  if($type === "register") {

    $name = filter_input(INPUT_POST, "name");
    $lastname = filter_input(INPUT_POST, "lastname");
    $email = filter_input(INPUT_POST, "email");
    $password = filter_input(INPUT_POST, "password");
    $confirmpassword = filter_input(INPUT_POST, "confirmpassword");


    // verificação de dados mínimos
    if($name && $lastname && $email && $password && $confirmpassword) {


      // Verificar se as senhas batem (conferem)
      if($password === $confirmpassword) {
        
        // Verificar se a senha, têm no mínimo 6 caracteres
        if(preg_match('/^[\w$@]{6,}$/', $password)) {

          // Verificar se o e-mail já está cadastrado no sistema
          if($userDao->findByEmail($email) === false) {

            $user = new User();
            
            // Criação de token e senha
            $userToken = $user->generateToken();
            $finalPassword = $user->generatePassword($password);


            $user->name = $name;
            $user->lastname = $lastname;
            $user->email = $email;
            $user->password = $finalPassword;
            $user->token = $userToken;

            $auth = true;

            $userDao->create($user, $auth);

          } else {

            // enviar menssagem de erro, usuário já existe
            $message->setMessage("Usuário já cadastrado, tente outro e-mail", "error", "back");

          }

        } else {
  
          // enviar menssagem de erro, senhas ter no mínimo 6 caracteres
          $message->setMessage("As senhas têm que ter no mínimo, de 6 caracteres", "error", "back");
  
        }
        

      } else {

        // enviar messagem de erro, senhas não batem(conferem)
        $message->setMessage("As senhas não são iguais.", "error", "back");

      }

    } else {

      // enviar messagem de erro, dos dados faltantes
      $message->setMessage("Por favor, preencha todos os campos.", "error", "back");

    }
    

    

  } else if($type === "login") {

    $email = filter_input(INPUT_POST, "email");
    $password = filter_input(INPUT_POST, "password");

    // print_r($userDao->authenticateUser($email, $password)); exit;

    // Tenta autenticar usuário
    if($userDao->authenticateUser($email, $password)) {

      // enviar mensagem de erro, Usuários ou senha incorretos
      $message->setMessage("Seja bem-vindo!.", "success", "index.php");

      // Redireciona  o usuário, caso não conseguir autenticar
    } else {

      // enviar mensagem de erro, Usuários ou senha incorretos
      $message->setMessage("Usuário e/ou senha incorretos.", "error", "back");

    }
    

  } else {

    // enviar mensagem de erro, Informações inválidas
    $message->setMessage("Informações inválidas.", "error", "index.php");

  }


?>


