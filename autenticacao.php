<?php 
  require_once "src/templates/header.php"; 
?>

  <main class="main">
    <div class="container login">
      <form action="<?= $BASE_URL ?>autenticacao_process.php" method="POST" class="login__form">

        <input type="hidden" name="type" value="login">

        <fieldset>
          <!-- <legend>Iniciar sessão</legend> -->

          <div class="login__container">

            <label for="email" >E-mail</label>
            <input type="text" class="login__autenticacao login__autenticacao--m-bottom" name="email" placeholder="Digite seu e-mail">

            <label for="password" >Senha</label>
            <input type="password" class="login__autenticacao" id="mypass" name="password" placeholder="Digite sua senha">
            <div class="ri-eye__container">
              
              <i class="ri-eye-fill showPass"></i>
              <i class="ri-eye-off-fill showPass" style="display:none;"></i>
            </div>

            <button type="submit" class="btn btn_default btn__blue">Entrar</button>

          </div>
        
        </fieldset>
      </form>
    </div>
  </main>
  <?php require_once "src/templates/contato.php"; ?>

<?php 
  require_once "src/templates/footer.php"; 
?>